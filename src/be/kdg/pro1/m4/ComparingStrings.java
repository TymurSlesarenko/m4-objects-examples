package be.kdg.pro1.m4;

public class ComparingStrings {
  public static void main(String[] args) {
    String one = "abc";
    String two = "abc";
    String three = new String("abc");

    System.out.println(one == two);
    System.out.println(one == three);
    System.out.println(two == three);

    System.out.println(one.equals(two));
    System.out.println(one.equals(three));
    System.out.println(two.equals(three));
  }
}
