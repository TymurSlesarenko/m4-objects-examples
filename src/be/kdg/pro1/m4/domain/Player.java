package be.kdg.pro1.m4.domain;

import java.util.Objects;

public class Player {

  private String name;
  private int lives;

  public Player(String name, int lives) {
    this.name = name;
    this.lives = lives;
  }

  public String getName() {
    return name;
  }

  public int getLives() {
    return lives;
  }

  public void extraLife() {
    lives++;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Player player)) return false;
    return Objects.equals(name, player.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }
}
