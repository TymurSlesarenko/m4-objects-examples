package be.kdg.pro1.m4;

import be.kdg.pro1.m4.domain.Player;

import java.util.*;


public class Reference {
  public static void main(String[] args) {
    // an object reference must be assigned to a variable before use
    Random generator;
    String name;


    // commenting the line below will make compilation fail
    generator=new java.util.Random();
    int random = generator.nextInt();

    //assignment of primitive types
    int i;
    int j;
    i = 7;
    j = i;  // j is now also 7
    i++;     // i is 8, j is still 7
    System.out.println(j);    // 7

    //assignment of reference types

    Player player1;
    Player player2;
    player1 = new Player("hunter25", 3);    // 3 lives
    player2 = player1;     // player2 also refers to "hunter25"
    player1.extraLife();
    System.out.println(player2.getLives());     // 4!








  }
}
