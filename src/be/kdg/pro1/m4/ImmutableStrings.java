package be.kdg.pro1.m4;

public class ImmutableStrings {
  public static void main(String[] args) {
    String s1 = "abc";
    s1.concat("def");
    System.out.println(s1);
    String s2 = s1.concat("def");
    System.out.println(s2);
  }
}
